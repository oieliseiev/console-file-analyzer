package model;

import java.util.List;

public class ProcessFile {
    private String name;
    private List<ProcessString> strings;
    private int stringCount;

    public ProcessFile(List<ProcessString> strings, String name) {
        this.strings = strings;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProcessString> getStrings() {
        return strings;
    }

    public void setStrings(List<ProcessString> strings) {
        this.strings = strings;
    }

    public int getStringCount() {
        return stringCount;
    }

    public void setStringCount(int stringCount) {
        this.stringCount = stringCount;
    }
}
