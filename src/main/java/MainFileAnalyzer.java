import model.ProcessFile;
import task.PersistFileTask;
import task.ProcessFileTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class MainFileAnalyzer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String path = null;
        while (path == null || path.isEmpty()) {
            System.out.println(ENTER_FILE_NAME_MSG);
            path = scanner.nextLine();
        }
        scanner.close();
        BlockingQueue<ProcessFile> queue = new ArrayBlockingQueue<>(1024);
        List<String> files = getAllFilesByPath(path, new ArrayList<String>());
        for (String filePath : files) {
            ProcessFileTask processFileTask = new ProcessFileTask(queue, filePath);
            new Thread(processFileTask).start();
        }
        PersistFileTask persistFileTask = new PersistFileTask(queue, files.size());
        new Thread(persistFileTask).start();
    }

    /**
     * Gets recursively all file names for given path.
     *
     * @param path  to get files
     * @param files list of file names
     * @return list of file names
     */
    private static List<String> getAllFilesByPath(String path, List<String> files) {
        File file = new File(path);
        if (file.isDirectory()) {
            File[] list = file.listFiles();
            if (list != null) {
                for (File f : list) {
                    if (f.isDirectory()) {
                        getAllFilesByPath(f.getAbsolutePath(), files);
                    } else {
                        files.add(f.getAbsolutePath());
                    }
                }
            }
        } else if (file.isFile()) {
            files.add(file.getAbsolutePath());
        } else {
            System.out.println(file.getName() + " is not a file.");
        }
        return files;
    }

    private static final String ENTER_FILE_NAME_MSG = "Please enter file path name to analyze:";
}
