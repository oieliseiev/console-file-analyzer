package task;

import model.ProcessFile;
import model.ProcessString;

import java.sql.*;
import java.util.concurrent.BlockingQueue;

public class PersistFileTask implements Runnable {

    private BlockingQueue<ProcessFile> queue;
    private final int filesCountToPersist;

    public PersistFileTask(BlockingQueue<ProcessFile> queue, int filesCountToPersist) {
        this.queue = queue;
        this.filesCountToPersist = filesCountToPersist;
    }

    @Override
    public void run() {
        try {
            initializeDatabase();
            int fileCounter = 0;
            while (fileCounter < filesCountToPersist) {
                persistFile(queue.take());
                fileCounter++;
            }
        } catch (InterruptedException e) {
            System.out.println("Failed to persist files: [" + e.getMessage() + "]");
        }
    }

    /**
     * Persist given file to database.
     *
     * @param processFile to persist
     */
    private void persistFile(ProcessFile processFile) {
        System.out.println("Persist file: " + processFile.getName());
        try (
                Connection connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
                PreparedStatement insertFileStatement = connection.prepareStatement(INSERT_NEW_FILE, Statement.RETURN_GENERATED_KEYS);
                PreparedStatement insertStringStatement = connection.prepareStatement(INSERT_NEW_STRING)
        ) {
            insertFileStatement.setString(1, processFile.getName());
            insertFileStatement.setInt(2, processFile.getStringCount());
            insertFileStatement.executeUpdate();
            long fileId;
            try (ResultSet generatedKeys = insertFileStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    fileId = generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
            int strCounter = 0;
            connection.setAutoCommit(false);
            for (ProcessString string : processFile.getStrings()) {
                insertStringStatement.setInt(1, string.getLength());
                insertStringStatement.setDouble(2, string.getAverageWordLength());
                insertStringStatement.setString(3, string.getMaxWord());
                insertStringStatement.setString(4, string.getMinWord());
                insertStringStatement.setLong(5, fileId);
                insertStringStatement.addBatch();
                strCounter++;
                if (strCounter % 1000 == 0 || strCounter == processFile.getStrings().size()) {
                    insertStringStatement.executeBatch();
                    try {
                        connection.commit();
                    } catch (SQLException e) {
                        connection.rollback();
                        throw e;
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("Failed to persist file: [" + processFile.getName() + "], message: [" + e.getMessage() + "]");
        }
    }

    /**
     * Initialize database with empty tables: PROCESS_FILE and PROCESS_STRING.
     */
    private void initializeDatabase() {
        try {
            Class.forName(DB_DRIVER);
            try (
                    Connection connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
                    Statement statement = connection.createStatement()
            ) {
                statement.executeUpdate(CREATE_FILE_TABLE);
                statement.executeUpdate(CREATE_STRING_TABLE);
            } catch (SQLException e) {
                System.out.println("Failed to initialize database: [" + e.getMessage() + "]");
            }
        } catch (ClassNotFoundException e) {
            System.out.println("Driver class not found: [" + DB_DRIVER + "]");
        }
    }

    private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_CONNECTION = "jdbc:h2:tcp://localhost/~/test";
    private static final String DB_USER = "sa";
    private static final String DB_PASSWORD = "";

    private static final String INSERT_NEW_FILE = "INSERT INTO PROCESS_FILE(NAME, STRING_COUNT) VALUES(?, ?)";
    private static final String INSERT_NEW_STRING = "INSERT INTO PROCESS_STRING(LENGTH, AVERAGE_WORD_LENGTH, " +
            "MAX_WORD, MIN_WORD, FILE_ID)" +
            " VALUES(?, ?, ?, ?, ?)";
    private static final String CREATE_FILE_TABLE = "CREATE TABLE IF NOT EXISTS PROCESS_FILE(" +
            "FILE_ID IDENTITY PRIMARY KEY, " +
            "NAME VARCHAR(255), " +
            "STRING_COUNT INT)";
    private static final String CREATE_STRING_TABLE = "CREATE TABLE IF NOT EXISTS PROCESS_STRING(" +
            "STRING_ID IDENTITY PRIMARY KEY, " +
            "LENGTH INT, " +
            "AVERAGE_WORD_LENGTH DOUBLE, " +
            "MAX_WORD VARCHAR(255), " +
            "MIN_WORD VARCHAR(255), " +
            "FILE_ID BIGINT, " +
            "FOREIGN KEY (FILE_ID) " +
            "REFERENCES PROCESS_FILE(FILE_ID))";
}
