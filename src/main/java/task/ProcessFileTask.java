package task;

import model.ProcessFile;
import model.ProcessString;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ProcessFileTask implements Runnable {
    private BlockingQueue<ProcessFile> queue;
    private final String filePath;

    public ProcessFileTask(BlockingQueue<ProcessFile> queue, String filePath) {
        this.queue = queue;
        this.filePath = filePath;
    }

    @Override
    public void run() {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            List<ProcessString> strings = new ArrayList<>();
            File fileToProcess = new File(filePath);
            ProcessFile file = new ProcessFile(strings, fileToProcess.getName());
            String line;
            while ((line = reader.readLine()) != null) {
                ProcessStringTask stringTask = new ProcessStringTask(line);
                Future<ProcessString> result = executor.submit(stringTask);
                try {
                    strings.add(result.get());
                } catch (InterruptedException | ExecutionException e) {
                    System.out.println("Error while processing line [" + line + "], message [" + e.getMessage() + "]");
                }
            }
            executor.shutdown();
            file.setStringCount(strings.size());
            queue.put(file);
        } catch (IOException e) {
            System.out.println("Failed to read file [" + filePath + "], message [" + e.getMessage() + "]");
        } catch (InterruptedException e) {
            System.out.println("Failed to put file to queue: [" + e.getMessage() + "]");
        }
    }
}
