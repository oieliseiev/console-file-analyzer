package task;

import model.ProcessString;

import java.util.concurrent.Callable;

public class ProcessStringTask implements Callable<ProcessString> {
    private final String line;

    public ProcessStringTask(String line) {
        this.line = line;
    }

    @Override
    public ProcessString call() throws Exception {
        ProcessString processString = new ProcessString(line.length());
        String[] words = line.split(BLANK);
        if (words.length > 0) {
            double sum = 0;
            String maxWord = words[0], minWord = words[0];
            for (String word : words) {
                if (minWord.length() > word.length()) minWord = word;
                if (maxWord.length() < word.length()) maxWord = word;
                sum += word.length();
            }
            processString.setMaxWord(maxWord);
            processString.setMinWord(minWord);
            processString.setAverageWordLength(sum / words.length);
        }
        return processString;
    }

    private static final String BLANK = " ";
}
