package task;

import model.ProcessFile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(value = Parameterized.class)
public class ProcessFileTaskTest {

    private BlockingQueue<ProcessFile> queue;

    @Parameterized.Parameter(value = 0)
    public String filePath;

    @Parameterized.Parameter(value = 1)
    public String fileName;

    @Parameterized.Parameter(value = 2)
    public int expectedStringCount;

    @Before
    public void init() {
        queue = new ArrayBlockingQueue<>(1024);
    }

    @Parameterized.Parameters(name = "{index}: testFile{0}) - fileName[{1}], expectedStringCount[{2}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"src/test/resources/test/emptyFile", "emptyFile", 0},
                {"src/test/resources/test/testFile1", "testFile1", 2}
        });
    }

    @Test
    public void testFile() {
        ProcessFileTask processFileTask = new ProcessFileTask(queue, filePath);
        Thread thread = new Thread(processFileTask);
        thread.start();
        try {
            thread.join();
            ProcessFile processFile = queue.take();
            assertEquals(fileName, processFile.getName());
            assertEquals(expectedStringCount, processFile.getStringCount());
        } catch (InterruptedException e) {
            fail();
        }
    }
}
