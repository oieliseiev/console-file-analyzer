package task;

import model.ProcessString;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(value = Parameterized.class)
public class ProcessStringTaskTest {

    private ExecutorService executor;

    @Parameterized.Parameter(value = 0)
    public String processString;

    @Parameterized.Parameter(value = 1)
    public String maxWord;

    @Parameterized.Parameter(value = 2)
    public String minWord;

    @Parameterized.Parameter(value = 3)
    public int length;

    @Parameterized.Parameter(value = 4)
    public double averageWordLength;

    @Before
    public void init() {
        executor = Executors.newSingleThreadExecutor();
    }

    @Parameterized.Parameters(name = "{index}: testString({0}) - maxWord[{1}], minWord[{2}], length[{3}], averageWordLength[{4}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"hello", "hello", "hello", 5, 5},
                {"hello thisIsMaximum and min: m", "thisIsMaximum", "m", 30, 5.2},
                {" hello", "hello", "", 6, 2.5},
                {"", "", "", 0, 0}
        });
    }

    @Test
    public void testSimpleString() {
        ProcessStringTask processStringTask = new ProcessStringTask(processString);
        Future<ProcessString> result = executor.submit(processStringTask);
        try {
            ProcessString string = result.get();
            assertEquals(maxWord, string.getMaxWord());
            assertEquals(minWord, string.getMinWord());
            assertEquals(length, string.getLength());
            assertEquals(averageWordLength, string.getAverageWordLength(), 0.1);
        } catch (InterruptedException | ExecutionException e) {
            fail();
        }
    }

    @After
    public void testMultipleString() {
        executor.shutdown();
    }
}
